//
//  ViewController.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/28.
//  Copyright © 2016年 XiaoGang. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "ViewController.h"
#import "TempleteShadowWindow.h"
#import "VEWindowManager.h"
#import "VEWindowAnimationNormal.h"

@interface ViewController ()<VEWindowDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initView{
    UIButton *openBtn = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    [openBtn setBackgroundColor:[UIColor redColor]];
    [openBtn setTitle:@"打开窗口" forState:UIControlStateNormal];
    [self.view addSubview:openBtn];
    [openBtn addTarget:self action:@selector(openBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];
}



-(void) openBtnOnClick:(UIButton*)sender{
    TempleteShadowWindow *window = [[TempleteShadowWindow alloc]init];
    //window.delegate = self;                                       //代理
    //window.animationObj = [[VEWindowAnimationNormal alloc] init]; //可自行修改动画
    //window.animationObj.duration_open = 0.4f;                     //修改动画的时长
    [VEWindowManager openWindow:window withParentView:self.view animation:true];
}

/**
 *  MARK:--------------------VEWindowDelegate--------------------
 */
-(void) veWindow_DidAppear:(VEWindowBase *)window{}
-(void) veWindow_DidDisappear:(VEWindowBase *)window{}


@end
