//
//  AppDelegate.h
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/28.
//  Copyright © 2016年 XiaoGang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

