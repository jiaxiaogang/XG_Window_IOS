//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol IVEWindowAnimation <NSObject>

-(void) animation_Close:(UIView *)animationObj completion:(void (^)(BOOL))completion;
-(void) animation_Open:(UIView *)animationObj completion:(void (^)(BOOL))completion;

@end


@interface VEWindowAnimationBase : NSObject <IVEWindowAnimation>

@property (assign, nonatomic) CGFloat duration_open;    //打开动画时间
@property (assign, nonatomic) CGFloat duration_close;   //关闭动画时间

@end
