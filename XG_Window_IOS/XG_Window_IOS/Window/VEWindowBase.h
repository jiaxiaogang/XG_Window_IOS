//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import <UIKit/UIKit.h>
#import "IVEWindow.h"

/**
 *  MARK:--------------------代理--------------------
 */
@class VEWindowBase;
@protocol VEWindowDelegate <NSObject>

-(void) veWindow_DidAppear:(VEWindowBase*)window;
-(void) veWindow_DidDisappear:(VEWindowBase*)window;

@end


/**
 *  MARK:--------------------VEWindowBase.h--------------------
 */
@class VEWindowAnimationBase;       //不能互相调用,否则报错;
@interface VEWindowBase : UIView <IVEWindow>

@property (weak, nonatomic) id<VEWindowDelegate> delegate;
@property (assign, nonatomic) BOOL animationSwitch;
@property (strong,nonatomic) VEWindowAnimationBase *animationObj;
-(UIView*) animation_OpenObj;       //获取打开动画部分的view;默认为全部窗口
-(UIView*) animation_CloseObj;      //获取关闭动画部分的view;默认为全部窗口

@end
