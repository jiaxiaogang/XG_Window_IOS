//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "TempleteShadowWindow.h"

@interface TempleteShadowWindow ()
@property (strong,nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIView *animationObjView;
@property (weak, nonatomic) IBOutlet UIButton *shadowBackBtn;

@end

@implementation TempleteShadowWindow

-(id) init{
    self = [super init];
    if (self) {
        [self initView];
        [self initData];
        [self initDisplay];
    }
    return self;
}

-(void) initView{
    [[NSBundle mainBundle] loadNibNamed:@"TempleteShadowWindow" owner:self options:nil];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    [self.containerView setFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    [self setFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    [self addSubview:self.containerView];
    
    //窗口content圆角
    [self.animationObjView.layer setCornerRadius:5];
    [self.animationObjView.layer setMasksToBounds:true];
  
}

-(void) initData{}

-(void) initDisplay{}

-(void) willAppear:(BOOL)animation{
    [super willAppear:animation];
    [self.shadowBackBtn setAlpha:0];
    [UIView animateWithDuration: 0.4f animations:^{
        [self.shadowBackBtn setAlpha:0.4f];
    }];
}

-(void) willDisappear:(BOOL)animation{
    [super willDisappear:animation];
    [UIView animateWithDuration: 0.3f animations:^{
        [self.shadowBackBtn setAlpha:0];
    }];
}

-(void) didDisappear{
    [super didDisappear];
}

/**
 *  MARK:--------------------onclick--------------------
 */
- (IBAction)closeBtnOnClick:(UIButton *)sender {
    [self Close];
}

- (IBAction)shadowBackBtnOnClick:(UIButton *)sender {
    [self Close];
}



/**
 *  MARK:--------------------override--------------------
 */
//获取打开动画部分的view;默认为全部窗口
-(UIView*) animation_OpenObj{
    return self.animationObjView;
}
//获取关闭动画部分的view;默认为全部窗口
-(UIView*) animation_CloseObj{
    return self.animationObjView;
}


@end
