//
//  TempleteShadowWindow.h
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "VEWindowBase.h"

/**
 *  MARK:--------------------示例窗口:--------------------
 *  
 *  带黑色背景的遮罩窗口
 */
@interface TempleteShadowWindow : VEWindowBase

@end
